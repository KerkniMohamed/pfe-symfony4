<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/15/19
 * Time: 11:23 AM
 */

namespace App\Service;


use App\Entity\Candidat;
use Doctrine\ORM\EntityManagerInterface;
use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
/**
 * @property string id
 * @property string commantaire
 */
class CandidatResolver
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    public function  getRecommandation(): array
    {
        $candidat= $this->em->getRepository(Candidat::class)->findAll();
        return $candidat;
    }
}