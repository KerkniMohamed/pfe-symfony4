<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/10/19
 * Time: 3:36 PM
 */

namespace App\Service;
use App\Entity\Offre;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @property string id
 * @property string date
 * @property string description
 */
class OffreResolver
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    public function getOffre() :  array
    {
        $offre = $this->em->getRepository(Offre::class)->findall();
        return $offre;
    }
}