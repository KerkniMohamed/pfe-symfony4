<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/15/19
 * Time: 11:23 AM
 */

namespace App\Service;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Technologie;
/**
 * @property string id
 * @property string tech
 * @property string description
 */
class TechnologieResolver
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    function getTechnologie(): array
    {
        $technologie =  $this->em->getRepository(Technologie::class)->findAll();
        return $technologie ;
    }

}