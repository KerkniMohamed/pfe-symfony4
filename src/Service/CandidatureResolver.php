<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/12/19
 * Time: 10:04 AM
 */

namespace App\Service;
use App\Entity\Candidature;
use Doctrine\ORM\EntityManagerInterface;

class CandidatureResolver
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getCandidature(): array
    {
        $candidature = $this->em->getRepository(Candidature::class)->findAll();
        return $candidature;
    }
}