<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/15/19
 * Time: 4:37 PM
 */

namespace App\Service;


use App\Entity\Feedback;
use Doctrine\ORM\EntityManagerInterface;
class FeedbackResolver
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    public function getOffre() :  array
    {
        $feedback = $this->em->getRepository(Feedback::class)->findall();
        return $feedback;
    }
}