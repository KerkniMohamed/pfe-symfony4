<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/8/19
 * Time: 9:32 AM
 */

namespace App\Service;
use App\Entity\Recommandation;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @property string id
 * @property string commantaire
 */

class RecommandationResolver
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    public function  getRecommandation(): array
    {
        $recommandation= $this->em->getRepository(Recommandation::class)->findAll();
        return $recommandation;
    }
}