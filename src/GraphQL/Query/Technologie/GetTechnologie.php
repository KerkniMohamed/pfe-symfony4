<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/11/19
 * Time: 10:22 AM
 */

namespace App\GraphQL\Query\Technologie;
use App\GraphQL\Type\Technologie\TechnologieType;
use App\Service\TechnologieResolver;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Type\AbstractType;



class GetTechnologie extends AbstractContainerAwareField
{
    public function build(FieldConfig $config)
    {
        $config->addArguments([
        ]);
    }
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /**@var TechnologieResolver $resolver*/
        $resolver=$this->container->get('technologie.resolver');
        return $resolver->getTechnologie();
    }
    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new ListType(new TechnologieType());
    }

}