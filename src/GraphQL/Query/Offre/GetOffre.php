<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/15/19
 * Time: 3:45 PM
 */

namespace App\GraphQL\Query\Offre;
use App\GraphQL\Type\Offre\OffreType;
use App\Service\OffreResolver;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Type\AbstractType;


class GetOffre extends AbstractContainerAwareField
{
    public function build(FieldConfig $config)
    {
        $config->addArguments([
        ]);
    }
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /**@var OffreResolver $resolver */
        $resolver = $this->container->get('offre.resolver');
        return $resolver;
    }
    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new ListType(new OffreType());
    }
}