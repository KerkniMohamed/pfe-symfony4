<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/13/19
 * Time: 4:29 PM
 */

namespace App\GraphQL\Query\User;
use App\GraphQL\Type\User\UserType;
use App\Service\UserResolver;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;


class UserConfig extends AbstractContainerAwareField
{

    public function build(FieldConfig $config)
    {
        $config->addArguments([
             // login et password
                    'username' => new StringType(),
                    'password' => new StringType(),

        ]);
    }
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /**@var UserResolver $resolver*/
          return $resolver=$this->container->get('user.resolver')->getUser($args) ;


    }

    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new StringType();
  //  return new ListType(new UserType());
    }
}