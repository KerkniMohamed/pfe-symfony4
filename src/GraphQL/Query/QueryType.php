<?php

namespace App\GraphQL\Query;
use App\GraphQL\Query\Candidat\GetCandidat;
use App\GraphQL\Query\Candidature\GetCandidature;
use App\GraphQL\Query\Feedback\GetFeeback;
use App\GraphQL\Query\Offre\GetOffre;
use App\GraphQL\Query\recommandation\RecommandationConfig ;
use App\GraphQL\Query\Technologie\GetTechnologie;
use App\GraphQL\Query\User\UserConfig;
use Youshido\GraphQL\Config\Object\ObjectTypeConfig;
use Youshido\GraphQL\Type\Object\AbstractObjectType;

class QueryType extends AbstractObjectType
{
    /**
     * @param ObjectTypeConfig $config
     *
     * @return mixed
     */
    public function build($config)
    {
        $config -> addFields([
                new RecommandationConfig(),
                new UserConfig(),
                new GetTechnologie(),
                new GetCandidat(),
                new GetOffre(),
                new GetFeeback(),
            ]
        );

    }
}


