<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/15/19
 * Time: 4:36 PM
 */

namespace App\GraphQL\Query\Feedback;
use App\GraphQL\Type\Feedback\FeedbackType;
use App\Service\FeedbackResolver;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Type\AbstractType;

class GetFeeback  extends AbstractContainerAwareField
{
    public function build(FieldConfig $config)
    {
        $config->addArguments([
        ]);
    }
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /**@var FeedbackResolver $resolver */
        $resolver = $this->container->get('feedback.resolver');
        return $resolver;
    }
    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new ListType(new FeedbackType());
    }
}