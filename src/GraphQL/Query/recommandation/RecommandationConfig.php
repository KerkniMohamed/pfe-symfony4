<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/8/19
 * Time: 8:41 AM
 */

namespace App\GraphQL\Query\recommandation;

use App\GraphQL\Type\Recommandation\RecommandationType;
use App\Service\RecommandationResolver;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Type\Scalar\StringType;


class RecommandationConfig extends AbstractContainerAwareField
{
    public function build(FieldConfig $config)
    {
        $config->addArguments([
            'id' =>  new IntType(),
        ]);
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        /**@var RecommandationResolver $resolver */
        $resolver = $this->container->get('recommandation.resolver');
        return $resolver->getRecommandation();
    }

    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new ListType(new RecommandationType());
    }
}
