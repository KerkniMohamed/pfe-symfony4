<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/15/19
 * Time: 2:10 PM
 */

namespace App\GraphQL\Query\Candidat;


use App\GraphQL\Type\Candidat\CandidatType;
use App\Service\CandidatResolver;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Type\AbstractType;

class GetCandidat extends AbstractContainerAwareField
{
    public function build(FieldConfig $config)
    {
        $config->addArguments([
        ]);
    }
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /**@var CandidatResolver $resolver */
        $resolver = $this->container->get('candidat.resolver');
        return $resolver;
    }
    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new ListType(new CandidatType());
    }
}