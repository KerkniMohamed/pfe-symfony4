<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/15/19
 * Time: 4:37 PM
 */

namespace App\GraphQL\Type\Feedback;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class FeedbackType extends AbstractObjectType
{
    public function build($config)
    {
        $config->addFields (
            [
                'id' => new StringType(),
                'nom' =>new StringType(),
                'prenom'=>new StringType(),
            ]
        );
    }
}