<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/15/19
 * Time: 10:57 AM
 */

namespace App\GraphQL\Type\Candidat;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class CandidatType extends AbstractObjectType
{
    public function build($config)
    {
        $config->addFields (
            [
                'id' => new StringType(),
                'nom' =>new StringType(),
                'prenom'=>new StringType(),
            ]
        );
    }
}