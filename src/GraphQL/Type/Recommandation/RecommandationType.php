<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/8/19
 * Time: 8:52 AM
 */

namespace App\GraphQL\Type\Recommandation;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;
class RecommandationType extends AbstractObjectType
{
    public function build($config)
    {
        $config->addFields (
            [
                'id'          => new StringType(),
                'commentaire' => new StringType()
            ]
        );
    }
}