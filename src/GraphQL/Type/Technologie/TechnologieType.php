<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/15/19
 * Time: 10:58 AM
 */

namespace App\GraphQL\Type\Technologie;


use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class TechnologieType extends AbstractObjectType
{
    public function build($config)
    {
        $config->addFields (
            [
                'id'   => new StringType(),
                'tech' => new StringType(),
                'desc' => new StringType()
            ]
        );
    }

}