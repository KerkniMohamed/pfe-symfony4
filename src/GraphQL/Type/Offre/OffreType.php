<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 4/15/19
 * Time: 4:29 PM
 */

namespace App\GraphQL\Type\Offre;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class OffreType extends AbstractObjectType
{

    public function build($config)
    {
        $config->addFields (
            [
                'id' => new StringType(),

            ]
        );
    }
}