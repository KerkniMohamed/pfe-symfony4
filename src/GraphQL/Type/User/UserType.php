<?php
/**
 * Created by PhpStorm.
 * User: kerkni
 * Date: 3/13/19
 * Time: 4:22 PM
 */

namespace App\GraphQL\Type\User;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class UserType extends AbstractObjectType
{
    public function build($config)
    {
        $config->addFields(
            [
                'token' => new StringType(),
            ]
        );
    }

}