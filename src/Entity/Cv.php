<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvRepository")
 */
class Cv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Technologie", mappedBy="cv")
     */
    private $technlogies;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidat", inversedBy="cvs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $candidat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidature", mappedBy="cv", orphanRemoval=true)
     */
    private $candidatures;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Recommandation", mappedBy="Cv", cascade={"persist", "remove"})
     */
    private $recommandation;


    public function __construct()
    {
        $this->technlogies = new ArrayCollection();
        $this->candidatures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|Technologie[]
     */
    public function getTechnlogies(): Collection
    {
        return $this->technlogies;
    }

    public function addTechnlogy(Technologie $technlogy): self
    {
        if (!$this->technlogies->contains($technlogy)) {
            $this->technlogies[] = $technlogy;
            $technlogy->setCv($this);
        }

        return $this;
    }

    public function removeTechnlogy(Technologie $technlogy): self
    {
        if ($this->technlogies->contains($technlogy)) {
            $this->technlogies->removeElement($technlogy);
            // set the owning side to null (unless already changed)
            if ($technlogy->getCv() === $this) {
                $technlogy->setCv(null);
            }
        }

        return $this;
    }

    public function getCandidat(): ?Candidat
    {
        return $this->candidat;
    }

    public function setCandidat(?Candidat $candidat): self
    {
        $this->candidat = $candidat;

        return $this;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidatures(): Collection
    {
        return $this->candidatures;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidatures->contains($candidature)) {
            $this->candidatures[] = $candidature;
            $candidature->setCv($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidatures->contains($candidature)) {
            $this->candidatures->removeElement($candidature);
            // set the owning side to null (unless already changed)
            if ($candidature->getCv() === $this) {
                $candidature->setCv(null);
            }
        }

        return $this;
    }

    public function getRecommandation(): ?Recommandation
    {
        return $this->recommandation;
    }

    public function setRecommandation(?Recommandation $recommandation): self
    {
        $this->recommandation = $recommandation;

        // set (or unset) the owning side of the relation if necessary
        $newCv = $recommandation === null ? null : $this;
        if ($newCv !== $recommandation->getCv()) {
            $recommandation->setCv($newCv);
        }

        return $this;
    }
}
