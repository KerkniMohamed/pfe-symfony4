<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatureRepository")
 */
class Candidature
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offre", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offre;



    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Feedback", inversedBy="candidature", cascade={"persist", "remove"})
     */
    private $feedback;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cv;

    public function __construct()
    {
        $this->entretients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getOffre(): ?Offre
    {
        return $this->offre;
    }

    public function setOffre(?Offre $offre): self
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * @return Collection|Entretient[]
     */
    public function getEntretients(): Collection
    {
        return $this->entretients;
    }

    public function addEntretient(Entretient $entretient): self
    {
        if (!$this->entretients->contains($entretient)) {
            $this->entretients[] = $entretient;
            $entretient->setCandidature($this);
        }

        return $this;
    }

    public function removeEntretient(Entretient $entretient): self
    {
        if ($this->entretients->contains($entretient)) {
            $this->entretients->removeElement($entretient);
            // set the owning side to null (unless already changed)
            if ($entretient->getCandidature() === $this) {
                $entretient->setCandidature(null);
            }
        }

        return $this;
    }

    public function getFeedback(): ?Feedback
    {
        return $this->feedback;
    }

    public function setFeedback(?Feedback $feedback): self
    {
        $this->feedback = $feedback;

        return $this;
    }

    public function getCv(): ?Cv
    {
        return $this->cv;
    }

    public function setCv(?Cv $cv): self
    {
        $this->cv = $cv;

        return $this;
    }
}
