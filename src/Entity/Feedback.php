<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeedbackRepository")
 */
class Feedback
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commentaire;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Candidature", cascade={"persist", "remove"})
     */
    private $candidature;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $noteRp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $noteRh;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getCandidature(): ?Candidature
    {
        return $this->candidature;
    }

    public function setCandidature(?Candidature $candidature): self
    {
        $this->candidature = $candidature;

        // set (or unset) the owning side of the relation if necessary
        $newFeedback = $candidature === null ? null : $this;
        if ($newFeedback !== $candidature->getFeedback()) {
            $candidature->setFeedback($newFeedback);
        }

        return $this;
    }

    public function getNoteRp(): ?int
    {
        return $this->noteRp;
    }

    public function setNoteRp(?int $noteRp): self
    {
        $this->noteRp = $noteRp;

        return $this;
    }

    public function getNoteRh(): ?int
    {
        return $this->noteRh;
    }

    public function setNoteRh(?int $noteRh): self
    {
        $this->noteRh = $noteRh;

        return $this;
    }
}
