<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301140643 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cv (id INT AUTO_INCREMENT NOT NULL, candidat_id INT NOT NULL, nom VARCHAR(255) NOT NULL, date DATE NOT NULL, etat VARCHAR(255) NOT NULL, INDEX IDX_B66FFE928D0EB82 (candidat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cv ADD CONSTRAINT FK_B66FFE928D0EB82 FOREIGN KEY (candidat_id) REFERENCES candidat (id)');
        $this->addSql('DROP TABLE planning');
        $this->addSql('ALTER TABLE technologie ADD cv_id INT NOT NULL');
        $this->addSql('ALTER TABLE technologie ADD CONSTRAINT FK_AD813674CFE419E2 FOREIGN KEY (cv_id) REFERENCES cv (id)');
        $this->addSql('CREATE INDEX IDX_AD813674CFE419E2 ON technologie (cv_id)');
        $this->addSql('ALTER TABLE candidature ADD feedback_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE candidature ADD CONSTRAINT FK_E33BD3B8D249A887 FOREIGN KEY (feedback_id) REFERENCES feedback (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E33BD3B8D249A887 ON candidature (feedback_id)');
        $this->addSql('ALTER TABLE candidat ADD email VARCHAR(255) NOT NULL, ADD tel VARCHAR(255) NOT NULL, ADD permis VARCHAR(255) NOT NULL, CHANGE infos adresse VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE technologie DROP FOREIGN KEY FK_AD813674CFE419E2');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE cv');
        $this->addSql('ALTER TABLE candidat ADD infos VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP adresse, DROP email, DROP tel, DROP permis');
        $this->addSql('ALTER TABLE candidature DROP FOREIGN KEY FK_E33BD3B8D249A887');
        $this->addSql('DROP INDEX UNIQ_E33BD3B8D249A887 ON candidature');
        $this->addSql('ALTER TABLE candidature DROP feedback_id');
        $this->addSql('DROP INDEX IDX_AD813674CFE419E2 ON technologie');
        $this->addSql('ALTER TABLE technologie DROP cv_id');
    }
}
