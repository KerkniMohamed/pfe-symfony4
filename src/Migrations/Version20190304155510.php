<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190304155510 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recommandation ADD rp_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE recommandation ADD CONSTRAINT FK_C7782A28B70FF80C FOREIGN KEY (rp_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C7782A28B70FF80C ON recommandation (rp_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recommandation DROP FOREIGN KEY FK_C7782A28B70FF80C');
        $this->addSql('DROP INDEX IDX_C7782A28B70FF80C ON recommandation');
        $this->addSql('ALTER TABLE recommandation DROP rp_id');
    }
}
