<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190410085107 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entretien (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, responsable VARCHAR(255) NOT NULL, pole VARCHAR(255) NOT NULL, tell VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE entretient');
        $this->addSql('ALTER TABLE offre ADD description VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entretient (id INT AUTO_INCREMENT NOT NULL, candidature_id INT NOT NULL, tel INT NOT NULL, date DATE NOT NULL, resp VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_E34739B4B6121583 (candidature_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE entretient ADD CONSTRAINT FK_E34739B4B6121583 FOREIGN KEY (candidature_id) REFERENCES candidature (id)');
        $this->addSql('DROP TABLE entretien');
        $this->addSql('ALTER TABLE offre DROP description');
    }
}
